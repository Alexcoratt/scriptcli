#ifndef NUMBER_HPP
#define NUMBER_HPP

#include <ostream>
#include <string>

template <typename T>
class Number {
public:
	using value_type = T;

private:
	T _value;

public:
	// Constructors
	inline Number(T const &value = 0) : _value{value} {}

	inline Number(Number const &other) : _value{other._value} {}
	inline Number(Number &&other) : _value{std::move(other._value)} {}

	// Assignment
	inline Number &operator=(Number const &other) { _value = other._value; return *this; }
	inline Number &operator=(Number &&other) { _value = std::move(other._value); return *this; }

	// Arithmetics
	inline void add(Number const &other) { _value += other; }
	inline void subtract(Number const &other) { _value -= other; }
	inline void multiply(Number const &other) { _value *= other; }
	inline void divide(Number const &other) { _value /= other; }

	// Arithmetic operators
	inline Number &operator+=(Number const &other) { add(other); return *this; }
	inline Number &operator-=(Number const &other) { subtract(other); return *this; }
	inline Number &operator*=(Number const &other) { multiply(other); return *this; }
	inline Number &operator/=(Number const &other) { divide(other); return *this; }

	// Converting
	inline std::string toString() const { return std::to_string(_value); }

	inline operator std::string() const { return toString(); }
	inline operator value_type() const { return _value; }
};

template <typename T> Number<T> operator+(Number<T> const &left, Number<T> const &right) { return Number<T>(left) += right; }
template <typename T> Number<T> operator-(Number<T> const &left, Number<T> const &right) { return Number<T>(left) -= right; }
template <typename T> Number<T> operator*(Number<T> const &left, Number<T> const &right) { return Number<T>(left) *= right; }
template <typename T> Number<T> operator/(Number<T> const &left, Number<T> const &right) { return Number<T>(left) /= right; }

template <typename T> std::ostream &operator<<(std::ostream &out, Number<T> const &num) { return out << num.toString(); }

#endif
