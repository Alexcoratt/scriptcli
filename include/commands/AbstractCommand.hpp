#ifndef ABSTRACT_COMMAND_HPP
#define ABSTRACT_COMMAND_HPP

#include <vector>
#include <memory>

template <typename TYPEDECL>
struct ComplexTypedecl {
	TYPEDECL decl;
	bool rvalue;
	bool constant;
};

// Typedecl operations
template <typename TYPEDECL>
bool operator==(ComplexTypedecl<TYPEDECL> left, ComplexTypedecl<TYPEDECL> right) { return left.decl == right.decl && left.pos == right.pos && left.constant == right.constant; }

template <typename TYPEDECL>
bool convertibleTo(ComplexTypedecl<TYPEDECL> source, ComplexTypedecl<TYPEDECL> target) {
	return !(
		(source.decl != target.decl) ||
		(source.rvalue && !target.rvalue) ||
		(source.constant && !source.rvalue && !target.constant && !target.rvalue)
	);
}


template <typename TYPEDECL>
class AbstractCommand {
public:
	using typedecl = ComplexTypedecl<TYPEDECL>;
	using declset = std::vector<typedecl>;

	inline virtual ~AbstractCommand() noexcept {}

	virtual declset getArgTypeDecl() const = 0;
	virtual typedecl getReturnTypeDecl() const = 0;

	virtual void *exec(void *) const = 0;
};

#endif
