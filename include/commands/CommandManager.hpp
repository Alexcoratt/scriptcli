#ifndef COMMAND_MANAGER_HPP
#define COMMAND_MANAGER_HPP

#include <memory>
#include <unordered_map>
#include <cstdlib>
#include <string>

#include "AbstractCommand.hpp"

class CommandManager {
public:
	enum class TypeDecl { VOID, INT };
	enum class ExecStatus { SUCCESS, FUNCTION_NOT_FOUND, WRONG_SET_OF_ARGUMENTS, UNKNOWN_ERROR };

	static std::unordered_map<std::string, std::unique_ptr<AbstractCommand<TypeDecl>>> commands;

	ExecStatus exec(int argc, char **argv, void *&res) {
		auto searchRes = commands.find(argv[0]);
		if (searchRes == commands.end())
			return ExecStatus::FUNCTION_NOT_FOUND;

		auto &cmd = searchRes->second;
		auto dset = cmd->getArgTypeDecl();

		if (argc - 1 < dset.size())
			return ExecStatus::WRONG_SET_OF_ARGUMENTS;

		int args[argc - 1];
		for (int i = 0; i < argc - 1; ++i) {
			int num = atoi(argv[i + 1]);
			if (num == 0 && !isdigit(argv[i + 1][0])) {
				void *subRes;
				auto subStatus = exec(argc - i - 1, argv + i + 1, subRes);
				if (subStatus != ExecStatus::SUCCESS)
					return subStatus;

				num = *(int *)subRes;
			}
			args[i] = num;
		}

		res = cmd->exec(args);
		if (res)
			return ExecStatus::SUCCESS;

		return ExecStatus::UNKNOWN_ERROR;
	}
};

inline std::unordered_map<std::string, std::unique_ptr<AbstractCommand<typename CommandManager::TypeDecl>>> CommandManager::commands{};

#endif
