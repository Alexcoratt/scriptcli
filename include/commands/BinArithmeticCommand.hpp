#ifndef BIN_ARITHMETIC_COMMAND_HPP
#define BIN_ARITHMETIC_COMMAND_HPP

#include "AbstractCommand.hpp"

enum class bin_operation { ADDTION, SUBTRACTION, MULTIPLICATION, DIVISION };

template <typename TYPEDECL, TYPEDECL DECL, typename VALUE_TYPE, bin_operation OPER_TYPE>
class BinArithmeticCommand : public AbstractCommand<TYPEDECL> {
public:
	using super = AbstractCommand<TYPEDECL>;
	using declset = typename super::declset;

	using typedecl = typename super::typedecl;
	using value_type = VALUE_TYPE;

	static constexpr bin_operation oper_type = OPER_TYPE;

	// Info
	inline constexpr declset getArgTypeDecl() const override {
		return {
			{DECL, true, true},
			{DECL, true, true}
		};
	}

	inline constexpr typedecl getReturnTypeDecl() const override { return {DECL, true, false}; }

	// Executing
	void *exec(void *params) const override {
		if (!params)
			return nullptr;

		auto left = *(value_type *)params;
		auto right = *((value_type *)params + 1);

		if constexpr (OPER_TYPE == bin_operation::ADDTION)
			return new value_type{left + right};
		else if constexpr (OPER_TYPE == bin_operation::SUBTRACTION)
			return new value_type{left - right};
		else if constexpr (OPER_TYPE == bin_operation::MULTIPLICATION)
			return new value_type{left * right};
		else
			return new value_type{left / right};
	}
};

#endif
