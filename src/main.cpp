#include <iostream>
#include <string>

#include "CommandManager.hpp"
#include "BinArithmeticCommand.hpp"

int main(int argc, char **argv) {
	if (argc == 1) {
		fprintf(stderr, "Usage: %s <function name> [args...]\n", argv[0]);
		return 1;
	}

	CommandManager cm;
	cm.commands["add"].reset(new BinArithmeticCommand<typename CommandManager::TypeDecl, CommandManager::TypeDecl::INT, int, bin_operation::ADDTION>);
	cm.commands["sub"].reset(new BinArithmeticCommand<typename CommandManager::TypeDecl, CommandManager::TypeDecl::INT, int, bin_operation::SUBTRACTION>);
	cm.commands["mul"].reset(new BinArithmeticCommand<typename CommandManager::TypeDecl, CommandManager::TypeDecl::INT, int, bin_operation::MULTIPLICATION>);
	cm.commands["div"].reset(new BinArithmeticCommand<typename CommandManager::TypeDecl, CommandManager::TypeDecl::INT, int, bin_operation::DIVISION>);

	void *res = nullptr;
	auto execResult = cm.exec(argc - 1, argv + 1, res);

	switch (execResult) {
		case CommandManager::ExecStatus::FUNCTION_NOT_FOUND:
			fprintf(stderr, "ERROR: function was not found\n");
			return 1;
		case CommandManager::ExecStatus::WRONG_SET_OF_ARGUMENTS:
			fprintf(stderr, "ERROR: entered wrong set of arguments\n");
			return 1;
		case CommandManager::ExecStatus::SUCCESS:
			printf("Result: %d\n", *(int *)res);
			break;
		default:
			fprintf(stderr, "ERROR: Unknown error\n");
			return 1;
	}

	return 0;
}
